/************************/plist/************************/
//path 读取当前程序定义好的provinces.plist省份城市文件  
//array数组的名称就叫province  
NSString *path = [[NSBundle mainBundle] pathForResource:@"provinces" ofType:@"plist"];  
NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:path];  
self.provincesContent = [NSArray arrayWithArray:[data objectForKey:@"province"]];

//沙盒路径plist文件的读取。
//注意，如果想添加新的数据，需要NSMutable类型的
NSArray *patharray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
NSString *path =  [patharray objectAtIndex:0];  
NSString *filepath=[path stringByAppendingPathComponent:@"provinces.plist"];
NSMutableArray *rootArray = [NSMutableArray arrayWithContentsOfFile:filepath];

/************************/NSUserDefaults/************************/
//NSUserDefaults读取
//获取标准函数对象 
//通过对象获取名称下NSMutableDictionary数据  
NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults]; 
NSMutableDictionary *defaultcoordinate = [defaults objectForKey:@"default_coordinate"];
NSString *currentCity = [defaultcoordinate objectForKey:@"c_name"];  

//NSUserDefaults写入，更新
//1添加id对象类型数据  
//2获取对象类型数据  
[defaults setObject:@"kaka" forKey:@"newdata"];
[defaults objectForKey:@"newdata"];
  
[defaults setDouble:2013 forKey:@"double"];
[defaults doubleForKey:@"double"];
  
[defaults setBool:NO forKey:@"BOOL"];
[defaults boolForKey:@"BOOL"];
  
[defaults setInteger:12 forKey:@"int"];
[defaults integerForKey:@"int"];
  
[defaults setFloat:0.23 forKey:@"float"];
[defaults floatForKey:@"float"];  
  
//NSUserDefaults删除
[defaults removeObjectForKey:@"newdata"];

//操作之后记得Synchronize同步操作，否则偶然会因为别的线程占大量内存而没有保存成功
[defaults synchronize];  